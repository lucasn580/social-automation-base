package br.com.sma.core.utilitario.calendario;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class Date {
    // ------------------------------ FIELDS ------------------------------
    private static final Logger log = LoggerFactory.getLogger(Date.class.getTypeName());
    private final DateTimeFormatter dateTimeDefault = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
    private final DateTimeFormatter dateDefault = DateTimeFormat.forPattern("dd/MM/yyyy");
    private final DateTimeFormatter timeDefault = DateTimeFormat.forPattern("HH:mm:ss");
    private DateTimeFormatter formato = null;
    private Integer somaAno = 0;
    private Integer somaMes = 0;
    private Integer somaDia = 0;
    private Integer somaHora = 0;
    private Integer somaMinuto = 0;
    private Integer somaSegundo = 0;
    // -------------------------- OTHER METHODS --------------------------

    /**
     * @return data e hora conforme o formato 'dd/MM/yyyy HH:mm:ss' ou formato pré-configurado.
     */
    public String getDateTime() {
        log.debug("Pegando data e hora");
        return (formato == null) ?
                plusDateTime(dateTimeDefault) :
                plusDateTime(formato);
    }

    /**
     * @return data conforme o formato 'dd/MM/yyyy' ou formato pré-configurado.
     */
    public String getDate() {
        log.debug("Pegando data");
        return (formato == null) ?
                plusDateTime(dateDefault) :
                plusDateTime(formato);
    }

    /**
     * @return hora conforme o formato 'HH:mm:ss' ou formato pré-configurado.
     */
    public String getTime() {
        log.debug("Pegando hora");
        return (formato == null) ?
                plusDateTime(timeDefault) :
                plusDateTime(formato);
    }

    /**
     * volta os valores para o pdrão:
     * formato = null;
     * somaAno = 0;
     * somaMes = 0;
     * somaDia = 0;
     * somaHora = 0;
     * somaMinuto = 0;
     * somaSegundo = 0;
     */
    public void reset() {
        log.debug("zerando os paramentros");
        formato = null;
        somaAno = 0;
        somaMes = 0;
        somaDia = 0;
        somaHora = 0;
        somaMinuto = 0;
        somaSegundo = 0;
    }

    /**
     * Formato pode ser da seguinte forma:
     * <p>
     * Symbol  Meaning                      Presentation  Examples
     * ------  -------                      ------------  -------
     * G       era                          text          AD
     * C       century of era (>=0)         number        20
     * Y       year of era (>=0)            year          1996
     * <p>
     * x       weekyear                     year          1996
     * w       week of weekyear             number        27
     * e       day of week                  number        2
     * E       day of week                  text          Tuesday; Tue
     * <p>
     * y       year                         year          1996
     * D       day of year                  number        189
     * M       month of year                month         July; Jul; 07
     * d       day of month                 number        10
     * <p>
     * a       halfday of day               text          PM
     * K       hour of halfday (0~11)       number        0
     * h       clockhour of halfday (1~12)  number        12
     * <p>
     * H       hour of day (0~23)           number        0
     * k       clockhour of day (1~24)      number        24
     * m       minute of hour               number        30
     * s       second of minute             number        55
     * S       fraction of second           number        978
     * <p>
     * z       time zone                    text          Pacific Standard Time; PST
     * Z       time zone offset/id          zone          -0800; -08:00; America/Los_Angeles
     * <p>
     * '       escape for text              delimiter
     * ''      single quote                 literal       '
     *
     * @param formato Formato deve serguir as especificações a cima.
     */
    public void setFormato(String formato) {
        this.formato = DateTimeFormat.forPattern(formato);
    }

    /**
     * Seta quantos anos deverão ser somados quando chamar uma função que irá retornar a data.
     * Caso for passado outro valor, o novo valor será somado.
     *
     * @param somaAno valor a ser somado
     */
    public void somarAno(Integer somaAno) {
        this.somaAno = somaAno;
    }

    /**
     * Seta quantos meses deverão ser somados quando chamar uma função que irá retornar a data.
     * Caso for passado outro valor, o novo valor será somado.
     *
     * @param somaMes valor a ser somado
     */
    public void somarMes(Integer somaMes) {
        this.somaMes = somaMes;
    }

    /**
     * Seta quantos dias deverão ser somados quando chamar uma função que irá retornar a data.
     * Caso for passado outro valor, o novo valor será somado.
     *
     * @param somaDia valor a ser somado
     */
    public void somarDia(Integer somaDia) {
        this.somaDia = somaDia;
    }

    /**
     * Seta quantos horas deverão ser somados quando chamar uma função que irá retornar a data.
     * Caso for passado outro valor, o novo valor será somado.
     *
     * @param somaHora valor a ser somado
     */
    public void somarHora(Integer somaHora) {
        this.somaHora = somaHora;
    }

    /**
     * Seta quantos minutos deverão ser somados quando chamar uma função que irá retornar a data.
     * Caso for passado outro valor, o novo valor será somado.
     *
     * @param somaMinuto valor a ser somado
     */
    public void somarMinuto(Integer somaMinuto) {
        this.somaMinuto = somaMinuto;
    }

    /**
     * Seta quantos segundos deverão ser somados quando chamar uma função que irá retornar a data.
     * Caso for passado outro valor, o novo valor será somado.
     *
     * @param somaSegundo valor a ser somado
     */
    public void somarSegundo(Integer somaSegundo) {
        this.somaSegundo = somaSegundo;
    }

    /**
     * Calcula a diferença em dias de duas datas passadas
     *
     * @param dataAntiga data inicial do período
     * @param dataAtual data final do período
     * @return diasPassados valor em dias que representa a diferença entre as datas
     */
    public long diferencaEntreDatas(String dataAntiga, String dataAtual) {
        long diasPassados = 0;
        try {
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            java.util.Date data1 = formato.parse(dataAntiga);
            java.util.Date data2 = formato.parse(dataAtual);
            long diff = data1.getTime() - data2.getTime();
            diasPassados = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            System.out.println(diasPassados);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diasPassados;
    }

    /**
     * metodos que irá somar todoas as datas e horas que foram setados anteriormente.
     *
     * @param formato Formato virá ou default ou de um formato pré-setado.
     * @return data formatada de acordo com as pré-configurações setadas.
     */
    private String plusDateTime(DateTimeFormatter formato) {
        log.debug("Pegando data e hora");
        LocalDateTime localDateTime = new LocalDateTime();
        return localDateTime.plusYears(somaAno).plusMonths(somaMes).plusDays(somaDia).plusHours(somaHora).plusMinutes(somaMinuto).plusSeconds(somaSegundo).toString(formato);
    }
    // -------------------------- END OF OTHER METHODS --------------------------
}