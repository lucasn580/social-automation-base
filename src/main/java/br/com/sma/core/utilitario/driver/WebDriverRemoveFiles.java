package br.com.sma.core.utilitario.driver;

import br.com.sma.core.utilitario.base.Util;
import br.com.sma.core.utilitario.properties.Data;

import java.io.File;

public class WebDriverRemoveFiles extends WebDriverFactory {
    // -------------------------- OTHERS METHODS --------------------------

    /**
     * Remove arquivos temporarios
     */
    public static void removeFiles() {
        String winTemp = "C:/Users/" + Data.get("user.name") + "/AppData/Local/Temp";
        String linuxTemp = "/tmp";


        if (Util.pegarSistemaOperacional().contains("Windows"))
            removeFiles(new File(winTemp));

        else removeFiles(new File(linuxTemp));
    }
    // -------------------------- PRIVATE METHODS --------------------------

    /**
     * Remove arquivos temporarios do Win
     *
     * @param temp Caminho para remover os arquivos
     */
    private static void removeFiles(File temp) {
        if (temp.isDirectory()) {
            log.debug(String.format("Entrando na pasta... %s", temp.getName()));
            File[] files = temp.listFiles();

            try {
                for (File file : files)
                    removeFiles(file);

                log.debug(String.format("Tentando apagar o arquivo... %s", temp.getName()));
                temp.delete();

            } catch (Exception e) {
                log.debug("Arquivos temporários excluidos");
                log.debug(String.valueOf(e));
            }
        }
    }
    // -------------------------- END OF OTHERS METHODS --------------------------
}
