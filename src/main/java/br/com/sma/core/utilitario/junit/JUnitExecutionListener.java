package br.com.sma.core.utilitario.junit;

import org.junit.AssumptionViolatedException;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JUnitExecutionListener extends RunListener {

    // ------------------------------ FIELDS ------------------------------
    private ListenerMessages listenerMessages = new ListenerMessages();
    private static boolean alreadyRunning = false;

    private static List<Description> m_allTestMethods = Collections.synchronizedList(new ArrayList<>());
    private static List<Failure> m_testFailures = Collections.synchronizedList(new ArrayList<>());
    private static List<Description> m_failedTests = Collections.synchronizedList(new ArrayList<>());
    private static List<Failure> m_assumptionFailures = Collections.synchronizedList(new ArrayList<>());
    private static List<Description> m_failedAssumptions = Collections.synchronizedList(new ArrayList<>());
    private static List<Description> m_ignoredTests = Collections.synchronizedList(new ArrayList<>());
    private static List<Description> m_retriedTests = Collections.synchronizedList(new ArrayList<>());
    private static List<Description> m_passedTests = Collections.synchronizedList(new ArrayList<>());

    /**
     * Called when an atomic test is about to be started.
     *
     * @param description the description of the test that is about to be run
     * (generally a class and method name)
     */
    @Override
    public void testStarted(Description description) {
        if(!alreadyRunning){

            if(ListenerAwareJUnitRunner.failedAttempts == 0)
                listenerMessages.Contador(getPassedTests().size());

            listenerMessages.logMessage();
            m_allTestMethods.add(description);

            alreadyRunning = true;
        }
    }

    /**
     * Called when an atomic test fails.
     *
     * @param failure describes the test that failed and the exception that was thrown
     */
    @Override
    public void testFailure(Failure failure) {
        if(getTestFailures().isEmpty()){
            m_testFailures.add(failure);
            m_failedTests.add(failure.getDescription());
            listenerMessages.logErrorMenssage();
        }
    }

    /**
     * Called when an atomic test flags that it assumes a condition that is
     * false
     *
     * @param failure
     *            describes the test that failed and the
     *            {@link AssumptionViolatedException} that was thrown
     */
    @Override
    public void testAssumptionFailure(Failure failure) {
        if(getFailedAssumptions().isEmpty()){
            m_assumptionFailures.add(failure);
            m_failedAssumptions.add(failure.getDescription());
        }
    }

    /**
     * Called when a test will not be run, generally because a test method is annotated
     * with {@link org.junit.Ignore}.
     *
     * @param description describes the test that will not be run
     */
    @Override
    public void testIgnored(Description description) {
        m_ignoredTests.add(description);
    }

    /**
     * Called when all tests finishes
     *
     * @param result describes the execution results
     */
    @Override
    public void testRunFinished(Result result) throws Exception {
        super.testRunFinished(result);
        if(listenerMessages.getResult() == null){
            if(result.wasSuccessful())
                listenerMessages.Contador(getPassedTests().size());

            listenerMessages.setResult(result);
            listenerMessages.showResults(getFailedTests().size());
        }
    }

    /**
     * Called when each test finished
     *
     * @param description the description of the test that is ended
     */
    @Override
    public void testFinished(Description description) throws Exception {
        super.testFinished(description);
        alreadyRunning = false;
    }

    /**
     * Get list of all tests that were run.
     *
     * @return list of all tests
     */
    public List<Description> getAllTestMethods() {
        return m_allTestMethods;
    }

    /**
     * Get list of passed tests.
     *
     * @return list of passed tests
     */
    private List<Description> getPassedTests() {
        m_passedTests.clear();
        m_passedTests.addAll(m_allTestMethods);
        m_failedTests.forEach(m_passedTests::remove);
        m_failedAssumptions.forEach(m_passedTests::remove);
        m_ignoredTests.forEach(m_passedTests::remove);
        m_retriedTests.forEach(m_passedTests::remove);
        return m_passedTests;
    }

    /**
     * Get list of test failure objects.
     *
     * @return list of failure objects
     */
    private List<Failure> getTestFailures() {
        return m_testFailures;
    }

    /**
     * Get list of failed tests.
     *
     * @return list of failed tests
     */
    private List<Description> getFailedTests() {
        return m_failedTests;
    }

    /**
     * Get list of assumption failures.
     *
     * @return list of assumption failures
     */
    public List<Failure> getAssumptionFailures() {
        return m_assumptionFailures;
    }

    /**
     * Get list of failed assumptions.
     *
     * @return list of failed assumptions
     */
    private List<Description> getFailedAssumptions() {
        return m_failedAssumptions;
    }

    /**
     * Get list of ignored tests.
     *
     * @return list of ignored tests
     */
    public List<Description> getIgnoredTests() {
        return m_ignoredTests;
    }

    /**
     * Get list of retried tests.
     *
     * @return list of retried tests
     */
    public List<Description> getRetriedTests() {
        return m_retriedTests;
    }
}