package br.com.sma.core.utilitario.driver;

import br.com.sma.core.utilitario.base.Util;
import br.com.sma.core.utilitario.properties.Data;
import org.jetbrains.annotations.Contract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WebDriverRecicle extends WebDriverFactory {
    // -------------------------- OTHERS METHODS --------------------------

    /**
     * Veririfca se o Driver atual esta aberto
     *
     * @return Retorna um Boolean se esta ou não aberto o driver
     */
    @Contract(pure = true)
    public static boolean verificarWebDriverAberto() {
        return webDriverInstance != null;
    }

    /**
     * Recicla o Driver Atual
     */
    public static void recicleWebDriver() {
        closeWebDriver();
    }

    /**
     * Envia um comando para matar o navebador atual
     */
    public static void matarNavegador() {
        if (Data.get("browser").contains("chrome"))
            processKill("chrome");

        else
            processKill(Data.get("browser"));
    }

    // -------------------------- PRIVATE METHODS --------------------------

    /**
     * Mata o navegator passado por parametro
     *
     * @param processName Nome do processo para matar.
     */
    private static void processKill(String processName) {
        if (Util.pegarSistemaOperacional().contains("Windows"))
            winProcessKill(processName);

        if (Util.pegarSistemaOperacional().contains("Linux"))
            linuxProcessKill(processName);

        if (Util.pegarSistemaOperacional().contains("Mac"))
            macprocesskill(processName);
    }

    /**
     * Mata o navegator passado por parametro para linux
     *
     * @param processName Nome do processo para matar.
     */
    private static void linuxProcessKill(String processName) {
        try {
            Runtime.getRuntime().exec("killall " + processName).waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Mata o navegator passado por parametro para mac
     *
     * @param processName Nome do processo para matar.
     */
    private static void macprocesskill(String processName) {
        if (processName.contains("chrome"))
            processName = "-9 Google\\ Chrome";

        try {
            Runtime.getRuntime().exec("killall " + processName).waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Mata o navegator passado por parametro para win
     *
     * @param processName Nome do processo para matar.
     */
    private static void winProcessKill(String processName) {
        String line;
        try {
            Process process = Runtime.getRuntime().exec("tasklist");
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while ((line = reader.readLine()) != null) {
                if (line.contains(processName)) {
                    Runtime.getRuntime().exec("taskkill /F /IM " + processName + ".exe");
                }
            }
        } catch (Exception e) {
            log.debug(String.format("Processo não foi morto... ='( %s", e));
        }
    }

    /**
     * Recicla o Driver
     */
    private static void closeWebDriver() {
        webDriverMap.forEach((key, webDriver) -> webDriver.quit());
        webDriverMap.clear();
    }
    // -------------------------- END OF OTHERS METHODS --------------------------
}