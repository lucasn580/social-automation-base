package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.BaseForPages;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.calendario.Date;
import br.com.sma.core.utilitario.entrada.Gravar;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.saida.Ler;
import org.openqa.selenium.By;

public class InstaPublicacao extends BaseForPages {
    //--------------------------------- FIELDS ------------------------------------

    //--------------------------------- OTHER METHODS ------------------------------------
    public void like(){
        log.debug("Like na publicação...");
        if(Wait.setWait(5000, () -> Wait.visibilityOfElementLocated(By.cssSelector("div > article > header")))){
            if(existsElement(By.xpath("//span[@aria-label='Curtir']"))){
                clicar(By.xpath("//span[@aria-label='Curtir']"));
                contador("Curtidas");

            } else log.debug("Publicação já foi curtida");
        }
    }

    public void follow(){
        log.debug("follow");
        if(Wait.setWait(5000, () -> Wait.visibilityOfElementLocated(By.cssSelector("div > article > header")))) {

            By btnSeguir = By.cssSelector("article > header > div > div > div > button");

            if (existsElement(btnSeguir)) {
                if(getText(btnSeguir).equals("Seguir")) {
                    clicar(btnSeguir);
                    contador("Seguindo");
                    gravarPerfil(1, "postagem");

                } else log.debug("Usuário já seguido");
            }
        }
    }

    private void followPorLista(int index){
        log.debug("followPorLista");
        if(existsElement(By.xpath("/html/body/div/div/div[2]/div/div/div["+index+"]/div/button"))){
            String text = getText(By.xpath("/html/body/div/div/div[2]/div/div/div["+index+"]/div/button"));

            if(text.equals("Seguir")){
                clicar(By.xpath("/html/body/div[4]/div/div[2]/div/div/div["+index+"]/div/button"));
                contador("Seguindo");
                gravarPerfil(index, "lista");

            } else log.debug("Usuário já seguido");
        }
    }

    public boolean verCurtidas(){
        log.debug("verCurtidas");
        if(!existsElement(By.xpath("//span[text()=' visualizações' or text()='1 visualização']")) && !existsElement(By.xpath("//button[text()='curtir isso']"))){

            if(existsElement(By.cssSelector("article > div > section > div > div > button")))
                clicar(By.cssSelector("article > div > section > div > div > button"));

            else if(existsElement(By.cssSelector("article > div > section > div > div > a")))
                clicar(By.cssSelector("article > div > section > div > div > a"));

            return true;

        } else log.debug("Não foi possível visualizar curtidas");

        return false;
    }

    public void seguirTodosOsCurtidores(){
        log.debug("seguirTodosOsCurtidores");

        if(Wait.setWait(5000, () -> Wait.elementToBeClickable(By.xpath("/html/body/div/div/div[2]/div/div/*")))){
            int curtidores = checkElements(By.xpath("/html/body/div/div/div[2]/div/div/*")).size();
            for(int i = 1; i <= curtidores; i++){
                followPorLista(i);

                if(i == curtidores)
                    curtidores = paginacao(curtidores);
            }
            fecharModal();
        }
    }

    private void entrarPerfilPorLista(int index){
        log.debug("entrarPerfilPorLista");
        if(Wait.setWait(5000, () -> Wait.visibilityOfElementLocated((By.xpath("/html/body/div/div/div[2]/div/div/div["+index+"]/div/div[1]/div")))))
            clicar(By.xpath("/html/body/div/div/div[2]/div/div/div["+index+"]/div/div[1]/div"));
    }

    public void visitarPerfisCurtidores(int perfis){
        log.debug("visitarPerfisCurtidores");

        if(verCurtidas()){

            int curtidores = 0;
            if(Wait.setWait(() -> Wait.elementToBeClickable(By.xpath("/html/body/div/div/div[2]/div/div/*"))))
                curtidores = checkElements(By.xpath("/html/body/div/div/div[2]/div/div/*")).size();

            String urlPubAtual = Data.get("urlAtual");

            if(perfis <= curtidores){
                entrarPerfilPorLista(perfis);
                if(new InstaPerfil().clicarPublicacao(1, 1)){
                    like();
                    fecharPost();
                }
                acessarUrl(urlPubAtual);
                deletarCookie("mid");

                visitarPerfisCurtidores(++perfis);
                fecharModal();
            }
            if(!pegarURLAtual().equals(Data.get("urlBuscada")))
                acessarUrl(Data.get("urlBuscada"));
        }
    }

    private int paginacao(int numero){
        log.debug("paginacao");

        scrollToBottomPage();
        Wait.setWait();
        int recount = checkElements(By.xpath("/html/body/div/div/div[2]/div/div/*")).size();
        if(numero < recount)
            return recount;
        else
            return numero;
    }

    public void fecharPost(){
        log.debug("fecharPost");
        if(existsElement(By.cssSelector("body > div > button")))
            clicar(By.cssSelector("body > div > button"));

        while(existsElement(By.cssSelector("body > div > button")))
            Wait.setWait();
    }

    public void fecharModal(){
        log.debug("fecharModal");
        if(existsElement(By.cssSelector("body > div > div > div > div > div > button")))
            clicar(By.cssSelector("body > div > div > div > div > div > button"));

        while(existsElement(By.cssSelector("body > div > div > div > div > div > button")))
            Wait.setWait();
    }

    private void contador(String tagName){
        if (Data.get(tagName) == null)
            Data.set(tagName, "1");
        else {
            Data.set(tagName, String.valueOf(Integer.parseInt(Data.get(tagName)) + 1));
        }
    }

    private void gravarPerfil(int index, String origem){
        int seguindo = 1;
        String conteudo = Ler.ler("src/main/resources/config/instagram.properties");

        while (conteudo.contains("PerfilSeguido" + seguindo))
            seguindo++;

        String text = "";
        if(origem.equals("postagem"))
            text = getText(By.xpath("(//a[contains(@class,'notranslate')])[" + index + "]"));
        else if(origem.equals("lista"))
            text = getAttribute(By.xpath("/html/body/div/div/div[2]/div/div/div["+index+"]/div/div/div/a"), "title");

        Gravar.gravar( conteudo + "\nPerfilSeguido" + seguindo + "=" + text
                + "\nDataPerfil" + seguindo + "=" + new Date().getDate(),"/instagram.properties", "UTF-8");
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}