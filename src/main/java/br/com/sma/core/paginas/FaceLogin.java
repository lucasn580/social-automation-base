package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.Page;
import br.com.sma.core.utilitario.properties.Data;
import org.openqa.selenium.By;

public class FaceLogin extends Page {
    // -------------------------- OTHER METHODS --------------------------
    public void loginFB() {
        log.debug("Acessando");
        acessarFB();

        log.debug("logando");
        logarFB();
    }

    public void logoffFB() {
        log.debug("Logoff");
        clicar(By.linkText("Logoff"));
    }

    private void acessarFB() {
        log.debug("Acessando Facebook");
        maximizarTela();
        acessarUrl(Data.get("urlInicial"));
    }

    private void logarFB() {
        log.debug("logarFB");
        preencherCampo(By.id("email"), Data.get("Email"));
        preencherCampo(By.name("pass"), Data.get("Senha"));
        clicar(By.xpath("//input[@value='Entrar']"));
    }
    // -------------------------- END OF OTHER METHODS --------------------------
}