package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.BaseForPages;
import org.openqa.selenium.By;

public class InstaBusca extends BaseForPages {
    //--------------------------------- FIELDS ------------------------------------
    private int tipoBusca;

    //--------------------------------- CONSTRUCTORS ------------------------------------
    public InstaBusca(String tipo){
        if(tipo.equals("Tag"))
            this.tipoBusca = 3;
        else if(tipo.equals("Local"))
            this.tipoBusca = 4;
    }

    //--------------------------------- OTHER METHODS ------------------------------------
    public int verificarTamanhoDoGrid() {
        log.debug("verificarTamanhoDoGrid");
        if(existsElement(By.cssSelector("#react-root > section > main > article > div:nth-child("+tipoBusca+") > div")))
            return checkElements(By.xpath("//*[@id=\"react-root\"]/section/main/article/div[2]/div/*")).size();
        else
            return 0;
    }

    public void clicarPublicacaoNovas(int linha, int coluna){
        log.info("clicarPublicacaoNovas linha: " + linha + " coluna: " + coluna);

        By element = By.cssSelector("#react-root > section > main > article > div:nth-child("+tipoBusca+") > div > div:nth-child("+linha+") > div:nth-child("+coluna+")");
        scrollToElementJS(element);
        if(existsElement(element))
            clicar(element);
    }

    public void clicarPublicacaoPrincipais(int linha, int coluna){
        log.info("clicarPublicacaoNovas linha: " + linha + " coluna: " + coluna);
        if(existsElement(By.cssSelector("#react-root > section > main > article > div > div > div > div:nth-child("+linha+") > div:nth-child("+coluna+")")))
            clicar(By.cssSelector("#react-root > section > main > article > div > div > div > div:nth-child("+linha+") > div:nth-child("+coluna+")"));

        deletarCookie("mid");
    }

    public int paginacaoPosts(int numero){
        log.debug("paginacaoPosts");

        scrollToBottomPage();
        int recount = checkElements(By.cssSelector("#react-root > section > main > article > div:nth-child("+tipoBusca+") > div > *")).size();
        if(numero < recount)
            return recount;
        else
            return numero;
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}