package br.com.sma.core.auxiliares.header;

import br.com.sma.core.utilitario.base.Page;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.properties.Data;
import org.openqa.selenium.By;

public class Header extends Page {
    // -------------------------- Botões --------------------------
    private void clicarMenuItem() {
        log.debug("Verificando se existe o header");

        if (Wait.waitFor(() -> Wait.presenceOfAllElementsLocatedBy(By.cssSelector("header[id='header']")))) {
            showElement("header[id='header']");

            log.debug("Esperando o header ficar visivel novamente");
            Wait.setWait(() -> Wait.visibilityOfAllElementsLocatedBy(By.cssSelector("#header > div")),
                    () -> Wait.visibilityOfAllElementsLocatedBy(By.cssSelector("#hdr-notification > li.nav-item.nav-item--fixed > span")));

            log.debug("Clicando no menu");
            checkElement(By.cssSelector("#hdr-notification > li.nav-item.nav-item--fixed")).click();
        }
    }

    // -------------------------- Links MenuItem--------------------------
    public void clicarLinkLogout() {
        log.debug("pegando a url para ver se tem .sma");
        if (pegarURLAtual().contains(".sma")) {

            log.debug("clicando no menu");
            clicarMenuItem();

            log.debug("verificando se existe o botao finalizar e clica nele se existir");
            if (Wait.setWait(() -> Wait.visibilityOfElementLocated(By.xpath("//a[@href='/FimSecao.asp']"))))
                checkElement(By.xpath("//a[@href='/FimSecao.asp']")).click();

            acessarUrl(Data.get("urlFinal"));
        }
    }

    // -------------------------- Assert --------------------------
    public void assertNomeUsuario(String nomeUsuario) {
       log.debug("Verificando se o nome do usuario esta aparecendo");
       Wait.presenceOfAllElementsLocatedBy(By.xpath("//a[@href=\"/" + nomeUsuario + "/\"]"));
    }
}