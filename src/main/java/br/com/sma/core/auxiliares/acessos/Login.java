package br.com.sma.core.auxiliares.acessos;

import br.com.sma.core.utilitario.base.Page;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.properties.Data;
import org.openqa.selenium.By;

public class Login extends Page {
    //--------------------------------- OTHER METHODS ------------------------------------

    public void logar(String loginName) {
        logar(loginName, null);
    }

    public void logar(String loginName, String url) {
        logar(loginName, null, url);
    }

    public void logar(String loginName, String senha, String url) {
        log.debug("Verificacoes");

        deletarCookies();
        maximizarTela();

        if (url == null) acessarUrl(Data.get("url.inicial"));
        else acessarUrl(url);

        if (loginName != null) {
            log.debug("Preenchendo campos");

            if (existsElement(By.xpath("//*[contains(@href, '/accounts/login/')]")))
                clicar(By.xpath("//*[contains(@href, '/accounts/login/')]"));

            preencherLogin(loginName);

            if (senha == null) preencherSenha();
            else preencherSenha(senha);

            if (!existsElement(By.xpath("//button[@disabled]")))
                clicar(By.xpath("//*[text()='Entrar']"));

            while (Wait.waitFor(1000, () -> Wait.visibilityOfElementLocated(By.id("slfErrorAlert")))){
                log.error(getText(By.id("slfErrorAlert")));
                logar(loginName, senha, url);
            }

            while (Wait.waitFor(1000, () -> Wait.visibilityOfElementLocated(By.cssSelector("//title['Facebook | Error']")))){
                log.error(getText(By.id("sorry")));
                voltarBrowser();
                logar(loginName, senha, url);
            }
        }
    }

    // -------------------------- PRIVATE METHODS --------------------------
    private void preencherLogin(String loginName) {
        Wait.setWait(() -> Wait.visibilityOfElementLocated(By.name("username")));
        clicar(By.name("username"));
        preencherCampo(By.name("username"), loginName);
    }

    private void preencherSenha() {
        preencherSenha(null);
    }

    private void preencherSenha(String senha) {
        if (senha == null) verificarCamposSenha(Data.get("Senha"));
        else verificarCamposSenha(senha);
    }

    private void verificarCamposSenha(String pass) {
        if (Wait.waitFor(() -> Wait.visibilityOfElementLocated(By.name("password"))))
            preencherCampo(By.name("password"), pass);

        else {
            clicar(By.name("password"));
            preencherCampo(By.name("password"), pass);
        }
    }

    public void mudarPassword(String loginName) {
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}