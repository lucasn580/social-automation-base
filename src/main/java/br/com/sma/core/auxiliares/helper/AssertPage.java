package br.com.sma.core.auxiliares.helper;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static br.com.sma.core.utilitario.base.Page.existsElement;
import static br.com.sma.core.utilitario.base.Wait.*;

public class AssertPage {
    // -------------------------- FILDS --------------------------
    private static final Logger log = LoggerFactory.getLogger(AssertPage.class.getSimpleName());

    // -------------------------- OTHER METHODS --------------------------

    /**
     * Verifica se o elemento existe na tela, deve passar o a mensagem de erro e o locator para verificar.
     *
     * @param mensagem Mensagem de erro
     * @param locator  Localizador do elemento
     */
    public void assertExist(String mensagem, By locator) {
        Assert.assertTrue(mensagem, existsElement(locator));
    }

    /**
     * Fazendo as verificações de página
     * Verificando se a pagina esta carregada
     * Verificando se a url ainda é a mesma
     * Verificando para esconder elementos
     * Verificacao de 500-100
     * azendo as verificações para páginas MVC
     */

    public void assertPage() {

        log.debug("Verificando se a url ainda é a mesma");
        checkURL();
    }
}