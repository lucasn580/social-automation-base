package br.com.sma.medias.facebook.run;

import br.com.sma.support.base.BaseForSuite;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {

})

public class AutomacaoFacebook extends BaseForSuite {
    @BeforeClass
    public static void setUp() {
        iniciar(AutomacaoFacebook.class);
    }
}
