package br.com.sma.medias.instagram.tests;

import br.com.sma.core.paginas.InstaBusca;
import br.com.sma.core.paginas.InstaInicial;
import br.com.sma.core.paginas.InstaPublicacao;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.support.base.BaseForInstagram;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static br.com.sma.support.finalizar.Final.finalizar;
import static br.com.sma.support.iniciar.Inicio.iniciar;

public class TC1SeguirCurtirNovasPostagensPorHashtag extends BaseForInstagram {

    @Before
    public void setUp(){
        iniciar(Data.get("Email"));
    }

    @Test
    public void seguirCurtirNovasPostagensPorHashtag(){

    }

    @After
    public void tearDown(){
        finalizar(getClass().getSimpleName());
    }
}
