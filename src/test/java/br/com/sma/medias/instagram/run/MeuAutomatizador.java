package br.com.sma.medias.instagram.run;

import br.com.sma.medias.instagram.tests.*;
import br.com.sma.support.base.BaseForSuite;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
//        TC1SeguirCurtirNovasPostagensPorHashtag.class,
//        TC2SeguirCurtirPrincipaisPostagensPorHashtag.class,
//        TC3SeguirQuemCurtiuNovasPostagensPorHashtag.class,
//        TC4SeguirQuemCurtiuPrincipaisPostagensPorHashtag.class,
//        TC5VisitarPerfilNovasPostagensPorHashtag.class,
//        TC6VisitarPerfilPrincipaisPostagensPorHashtag.class,

//        TC7SeguirCurtirNovasPostagensPorLocal.class,
//        TC8SeguirCurtirPrincipaisPostagensPorLocal.class,
//        TC9SeguirQuemCurtiuNovasPostagensPorLocal.class,
//        TC10SeguirQuemCurtiuPrincipaisPostagensPorLocal.class,
//        TC11VisitarPerfilNovasPostagensPorLocal.class,
//        TC12VisitarPerfilPrincipaisPostagensPorLocal.class

})

public class MeuAutomatizador extends BaseForSuite {
    @BeforeClass
    public static void setUp() {
        iniciar(MeuAutomatizador.class);
    }
}