package br.com.sma.support.iniciar;

import br.com.sma.core.auxiliares.acessos.Login;
import br.com.sma.core.auxiliares.header.Header;
import br.com.sma.core.paginas.FaceLogin;
import br.com.sma.core.paginas.InstaInicial;
import br.com.sma.core.utilitario.base.Page;
import br.com.sma.core.utilitario.driver.WebDriverFactory;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.support.base.BaseForTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Inicio extends BaseForTests {
    // ------------------------------ FIELDS ------------------------------
    private static final Logger log = LoggerFactory.getLogger(Page.class.getSimpleName());

    // ------------------------------ OTHERS METHODS ------------------------------
    public static void iniciar(String nomeUsuario) {
        log.debug("Iniciando Driver");
        WebDriverFactory.criarNovoWebDriver();

        if (nomeUsuario != null) {
            log.debug("Logando...");
            Login login = new Login();
            login.logar(nomeUsuario);

            log.debug("Verificando popup");
            InstaInicial inicial = new InstaInicial();
            inicial.verificarPopUpNotificacoes();

            log.debug("Fazendo assert do usuario");
            Header header = new Header();
            header.assertNomeUsuario(Data.get("User"));
        }
    }

    public static void iniciarFacebook() {
        FaceLogin face = new FaceLogin();

        log.debug("Iniciando Driver");
        WebDriverFactory.criarNovoWebDriver();

        face.loginFB();
    }
    // ------------------------------ END OF OTHERS METHODS ------------------------------
}
