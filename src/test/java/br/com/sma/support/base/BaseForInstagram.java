package br.com.sma.support.base;

import br.com.sma.core.utilitario.properties.Data;

public class BaseForInstagram extends BaseForTests {
    // ------------------------------ FIELDS ------------------------------

    //--------------------------------- CONSTRUCTOR ------------------------------------
    protected BaseForInstagram() {
        Data.getResourceProperties("config/instagram.properties");
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}
