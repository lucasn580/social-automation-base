package br.com.sma.support.base;

import br.com.sma.core.utilitario.properties.Data;

public class BaseForFacebook extends BaseForTests {
    // ------------------------------ FIELDS ------------------------------

    //--------------------------------- CONSTRUCTOR ------------------------------------
    protected BaseForFacebook() {
        Data.getResourceProperties("config/facebook.properties");
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}