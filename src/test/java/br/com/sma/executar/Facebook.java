package br.com.sma.executar;

import br.com.sma.medias.facebook.run.AutomacaoFacebook;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        AutomacaoFacebook.class
})

public class Facebook {
}